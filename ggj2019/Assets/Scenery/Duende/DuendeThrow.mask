%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: DuendeThrow
  m_Mask: 00000000000000000000000000000000000000000100000001000000010000000100000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 0
  - m_Path: Armature/Bone
    m_Weight: 0
  - m_Path: Armature/Bone/Bone.002
    m_Weight: 0
  - m_Path: Armature/Bone/Bone.002/Bone.002_L.004
    m_Weight: 0
  - m_Path: Armature/Bone/Bone.002/Bone.002_L.004/Bone.002_L.005
    m_Weight: 0
  - m_Path: Armature/Bone/Bone.002/Bone.002_L.004/Bone.002_L.005/Bone.002_L.006
    m_Weight: 0
  - m_Path: Armature/Bone/Bone.002/Bone.002_L.004/Bone.002_L.005/Bone.002_L.006/Bone.002_L.006_end
    m_Weight: 0
  - m_Path: Armature/Bone/Bone.002/Bone.002_R.004
    m_Weight: 0
  - m_Path: Armature/Bone/Bone.002/Bone.002_R.004/Bone.002_R.005
    m_Weight: 0
  - m_Path: Armature/Bone/Bone.002/Bone.002_R.004/Bone.002_R.005/Bone.002_R.006
    m_Weight: 0
  - m_Path: Armature/Bone/Bone.002/Bone.002_R.004/Bone.002_R.005/Bone.002_R.006/Bone.002_R.006_end
    m_Weight: 0
  - m_Path: Armature/Bone/Bone.002/Bone.005
    m_Weight: 1
  - m_Path: Armature/Bone/Bone.002/Bone.005/Bone.002_L.001
    m_Weight: 1
  - m_Path: Armature/Bone/Bone.002/Bone.005/Bone.002_L.001/Bone.002_L.002
    m_Weight: 1
  - m_Path: Armature/Bone/Bone.002/Bone.005/Bone.002_L.001/Bone.002_L.002/Bone.002_L.003
    m_Weight: 1
  - m_Path: Armature/Bone/Bone.002/Bone.005/Bone.002_L.001/Bone.002_L.002/Bone.002_L.003/Bone.002_L.003_end
    m_Weight: 1
  - m_Path: Armature/Bone/Bone.002/Bone.005/Bone.002_R.001
    m_Weight: 1
  - m_Path: Armature/Bone/Bone.002/Bone.005/Bone.002_R.001/Bone.002_R.002
    m_Weight: 1
  - m_Path: Armature/Bone/Bone.002/Bone.005/Bone.002_R.001/Bone.002_R.002/Bone.002_R.003
    m_Weight: 1
  - m_Path: Armature/Bone/Bone.002/Bone.005/Bone.002_R.001/Bone.002_R.002/Bone.002_R.003/Bone.002_R.003_end
    m_Weight: 1
  - m_Path: Armature/Bone/Bone.002/Bone.005/Bone.003
    m_Weight: 1
  - m_Path: Armature/Bone/Bone.002/Bone.005/Bone.003/Bone.004
    m_Weight: 1
  - m_Path: Armature/Bone/Bone.002/Bone.005/Bone.003/Bone.004/Bone.004_end
    m_Weight: 1
  - m_Path: Duende
    m_Weight: 0
