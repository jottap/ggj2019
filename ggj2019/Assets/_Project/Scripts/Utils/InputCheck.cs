﻿using UnityEngine;

public class InputCheck : MonoBehaviour
{

    void Update()
    {
        try
        {
            if (Input.anyKey)
            {
                System.Array values = System.Enum.GetValues(typeof(KeyCode));
                foreach (KeyCode code in values)
                {
                    if (Input.GetKeyDown(code)) { print(System.Enum.GetName(typeof(KeyCode), code)); }
                }
            }
        }
        catch (System.Exception)
        {

            throw;
        }
    }
}
