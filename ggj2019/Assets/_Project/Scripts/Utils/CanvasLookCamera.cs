﻿using UnityEngine;

public class CanvasLookCamera : MonoBehaviour
{
    void Start()
    {
        transform.LookAt(transform.position + Camera.main.transform.rotation * Vector3.forward, Camera.main.transform.rotation * Vector3.up);
    }
}
