﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SelectPlayer : MonoBehaviour
{
    #region Variables
    [Header("Settings")]

    [SerializeField]
    private string playerIndex;
    public string PlayerIndex { get => playerIndex; set => playerIndex = value; }

    [SerializeField]
    private Image image;

    [SerializeField]
    private Color colorPlayer;
    public Color ColorPlayer { get => colorPlayer; set => colorPlayer = value; }

    [SerializeField]
    private Image imageCharacter;

    [SerializeField]
    private TextMeshProUGUI textSelect;

    #endregion

    public void SelectedPlayer(Color color, int playerIndexNumber)
    {
        ColorPlayer = color;
        PlayerIndex = string.Format("P{0}", playerIndexNumber);
        imageCharacter.gameObject.SetActive(true);
        image.color = color;
        textSelect.gameObject.SetActive(false);
    }
}
