﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StartPlayer : MonoBehaviour
{

    #region Viariables

    [Header("Settings")]
    [SerializeField]
    private TextMeshProUGUI textSelected;

    [SerializeField]
    private List<Color> colorsPlayers = new List<Color>();

    [SerializeField]
    private List<SelectPlayer> selectedPlayers = new List<SelectPlayer>();

    [SerializeField]
    private List<SelectPlayer> selectedPlayersActive = new List<SelectPlayer>();

    [SerializeField]
    private KeyCode keyCodeStartGame;

    [Header("Telas")]
    [SerializeField]
    private GameObject tutorial;

    [SerializeField]
    private GameObject startMenu;

    #endregion

    private void Start()
    {
        tutorial.SetActive(true);
        selectedPlayersActive.Clear();
    }

    void Update()
    {
        if (GameManager.Instance.statusGame == StatusGame.Tutorial)
        {
            if (Input.GetKeyDown(keyCodeStartGame))
            {
                NextTutorial();
            }
        }

        if (GameManager.Instance.statusGame == StatusGame.MenuStart)
        {
            if (Input.anyKey)
            {
                for (int i = 1; i <= 4; i++)
                {
                    if (Input.GetButtonDown(string.Format("P{0}_Start", i)))
                    {
                        selectedPlayers[i - 1].SelectedPlayer(colorsPlayers[i - 1], i);
                        selectedPlayersActive.Add(selectedPlayers[i - 1]);
                    }
                }
                if (selectedPlayersActive.Count >= 1)
                {
                    textSelected.text = string.Format("Press A for Start Game", keyCodeStartGame.ToString());
                }
            }

            if (Input.GetKeyDown(keyCodeStartGame) && GameManager.Instance.statusGame == StatusGame.MenuStart && selectedPlayersActive.Count >= 1)
            {
                GameManager.Instance.StartPlayer(selectedPlayersActive);
                gameObject.SetActive(false);
            }
        }

    }

    public void NextTutorial()
    {
        Debug.Log("NextTutorial");

        if (TutorialManager.Instance.indexTutorial == TutorialManager.Instance.tutoriaisPanel.Count - 1)
        {
            tutorial.SetActive(false);
            startMenu.SetActive(true);
            GameManager.Instance.statusGame = StatusGame.MenuStart;
        }
        else
        {
            TutorialManager.Instance.tutoriaisPanel[TutorialManager.Instance.indexTutorial].SetActive(false);
            TutorialManager.Instance.indexTutorial++;
            TutorialManager.Instance.tutoriaisPanel[TutorialManager.Instance.indexTutorial].SetActive(true);
        }

    }

}
