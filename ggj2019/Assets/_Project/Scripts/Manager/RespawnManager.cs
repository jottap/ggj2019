﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnManager : MonoBehaviour
{
    #region Variables

    public static RespawnManager Instance;

    [Header("Settings")]
    [SerializeField]
    private float timeRespawn;

    [SerializeField]
    private GameObject prefabEgg;

    [SerializeField]
    private int amountMax;

    [Header("Controller")]

    [SerializeField]
    private int amountRespawn;

    [SerializeField]
    public List<GameObject> eggs = new List<GameObject>();

    #endregion

    private void Start()
    {
        Instance = this;
    }

    public void InitSpawn()
    {
        StartCoroutine(RespawnEgg_Coroutine());
    }

    private IEnumerator RespawnEgg_Coroutine()
    {
        RespawnEgg();

        yield return new WaitForSeconds(timeRespawn);

        StartCoroutine(RespawnEgg_Coroutine());
    }

    [ContextMenu(" Respawn Egg ")]
    public void RespawnEgg()
    {
        if (amountRespawn >= amountMax)
            return;

        GameObject eggClone = Instantiate(prefabEgg, this.transform.position, Quaternion.identity);
        eggClone.transform.SetParent(this.transform);
        eggs.Add(eggClone);
        amountRespawn++;
    }

    public void UseEgg(GameObject egg)
    {
        eggs.Remove(egg);
        amountRespawn--;
    }

}
