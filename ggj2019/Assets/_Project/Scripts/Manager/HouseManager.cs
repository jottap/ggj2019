﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HouseManager : MonoBehaviour
{
    #region Variables

    [Header("Settings")]
    [SerializeField]
    private string player;

    public string Player { get => player; set => player = value; }

    [SerializeField]
    private SelectPlayer selectPlayer;

    [SerializeField]
    public GameObject spawn;

    [SerializeField]
    private Image colorPlayer;

    [Header("Controllers")]
    [SerializeField]
    private int Hp;

    [SerializeField]
    private int MaxHp;

    [SerializeField]
    private int amountValue;
    public int AmountValue { get => amountValue; set { amountValue = value; amountTxt.text = amountValue.ToString(); } }

    [SerializeField]
    private Image activeAction3;

    [SerializeField]
    private bool isActiveAction3;

    [SerializeField]
    private TextMeshProUGUI amountTxt;

    public Action<int, SelectPlayer> HouseScore;

    #endregion

    public void SetEgg(int amountValueAux)
    {
        AmountValue += amountValueAux;

        if (HouseScore != null)
        {
            HouseScore(AmountValue, selectPlayer);
        }
    }

    public void InitHouse(SelectPlayer player, string playerIndex, Color colorPlayerAux)
    {
        selectPlayer = player;
        Player = playerIndex;
        colorPlayer.color = colorPlayerAux;
    }

}
