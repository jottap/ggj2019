﻿using UnityEngine;

public class ControllerManager : MonoBehaviour
{
    #region Variables

    [Header("Settings")]
    [SerializeField]
    private string playerController;
    public string PlayerController { get => playerController; set => playerController = value; }

    [Header("Controller")]
    [SerializeField]
    private float vel = 5.0f;

    [SerializeField]
    private float rotVel = 15.0f;

    [SerializeField]
    private bool canMove = true;
    public bool CanMove { get => canMove; set => canMove = value; }

    [SerializeField]
    public SelectPlayer selectPlayer;

    #endregion

    void FixedUpdate()
    {
        if (CanMove)
            MovePlayer();
    }

    private void MovePlayer()
    {
        if (GameManager.Instance.statusGame == StatusGame.Gameplay)
        {

            float mHor = Input.GetAxis(string.Format("{0}_Horizontal", PlayerController)) * vel * Time.deltaTime;
            float mVer = Input.GetAxis(string.Format("{0}_Vertical", PlayerController)) * vel * Time.deltaTime * -1;

            Vector3 movement = new Vector3(mHor, 0, mVer);
            transform.Translate(movement * vel * Time.deltaTime, Space.World);

            if (movement != Vector3.zero)
            {
                GetComponent<PlayerManager>().animator.SetFloat("speed", 1);
                transform.rotation = Quaternion.LookRotation(movement);
            }
            else
            {
                GetComponent<PlayerManager>().animator.SetFloat("speed", 0);
            }
        }

    }


}
