﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    #region Variables

    [Header("Settings")]
    [SerializeField]
    private int power;

    [SerializeField]
    private int powerImpulse;

    [Header("Controllers")]
    [SerializeField]
    private bool isGround;

    public bool IsGround { get => isGround; set => isGround = value; }

    [SerializeField]
    public string PlayerTarget;

    #endregion

    public void Fire(Vector3 vector3)
    {
        GetComponent<Rigidbody>().AddForce(vector3 * power);
    }

    private void Update()
    {
        if (transform.position.y > 2)
        {
            //transform.position = new Vector3(transform.position.x, 2);
            //GetComponent<Rigidbody>().mass = 5;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name.Contains("Player"))
        {
            CollisionPlayer(other);
        }
    }

    private void CollisionPlayer(Collider other)
    {
        if (other.GetComponent<PlayerManager>().PlayerNumber.Equals(PlayerTarget))
        {
            PlayerTarget = string.Empty;
            GetComponent<BoxCollider>().isTrigger = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.LogFormat(" Collision {0} ", collision.gameObject.name);

        if (collision.gameObject.name.Contains("ground"))
        {
            GetComponent<BoxCollider>().isTrigger = true;
            GetComponent<Rigidbody>().useGravity = false;
            GetComponent<Rigidbody>().isKinematic = true;
            IsGround = true;
        }

        if (collision.gameObject.name.Contains("Player") && !collision.gameObject.GetComponent<PlayerManager>().PlayerNumber.Equals(PlayerTarget))
        {
            collision.gameObject.GetComponent<PlayerManager>().Stun();
            this.GetComponent<Rigidbody>().AddForce(-this.transform.forward * powerImpulse, ForceMode.Impulse);
            collision.gameObject.GetComponent<Rigidbody>().AddForce(this.transform.forward * powerImpulse, ForceMode.Impulse);
        }

    }

}
