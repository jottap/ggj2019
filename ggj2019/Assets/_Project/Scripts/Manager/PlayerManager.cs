﻿/*
 * 
 * 
 * Action1 : Pega Objeto
 * Action2 : Atira objeto
 * Action3 : Atualiza construcao
 * 
 * 
 * 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    #region Variables

    AudioSource audioData;

    [Header("Settings")]
    [SerializeField]
    private int amountProjectiles;

    [SerializeField]
    private string playerNumber;

    public string PlayerNumber { get => playerNumber; set => playerNumber = value; }

    [SerializeField]
    private float stunTimeMax;

    [SerializeField]
    public Animator animator;

    [Header("Aim")]

    [SerializeField]
    private GameObject aim;

    [SerializeField]
    private LayerMask mask;

    [Header("Pivot")]
    [SerializeField]
    private GameObject pivot1Egg;

    [SerializeField]
    private List<GameObject> pivot2Egg;

    [Header("Projectiles")]
    [SerializeField]
    private List<GameObject> eggProjectiles = new List<GameObject>();

    [SerializeField]
    private GameObject projectileTarget;

    public GameObject ProjectileTarget
    {
        get
        {
            return projectileTarget;
        }
        set
        {
            projectileTarget = value;
        }
    }

    public int AmountProjectiles
    {
        get => amountProjectiles;
        set
        {
            animator.SetInteger("amount", value);
            amountProjectiles = value;
        }
    }

    [Header("Controller")]
    [SerializeField]
    private float stunResistence = 1;

    [SerializeField]
    private float stunResistenceTime;

    [SerializeField]
    private Coroutine stunCoroutine;

    #endregion

    //Pivot para um egg
    //Pivot para 2 egg

    private void Start()
    {
        PlayerNumber = GetComponent<ControllerManager>().PlayerController;
        audioData = GetComponent<AudioSource>();



    }

    private void FixedUpdate()
    {
        if (Input.GetButtonDown(string.Format("{0}_Action2", PlayerNumber)) && AmountProjectiles == 1)
        {
            Debug.Log(" _Action2 ");
            Fire();
        }
    }

    #region Trigger

    private void OnTriggerStay(Collider other)
    {
        if (other.name.Contains("Egg") && ProjectileTarget == null && !eggProjectiles.Contains(other.gameObject))
        {
            ProjectileTarget = other.gameObject;
        }

        if (other.name.Contains("Egg") && other.GetComponent<Projectile>().IsGround && other.gameObject == ProjectileTarget)
        {
            //Debug.Log("FIRE EGG");
            if (Input.GetButtonDown(string.Format("{0}_Action1", PlayerNumber)))
            {
                if (AmountProjectiles > 1)
                {
                    return;
                }
                else
                {
                    GetProjectiles(other);
                }

                //Destroy(other.gameObject);
            }
            //Debug.Log("OnTriggerStay EGG");
        }

        if (other.name.Contains("House") && AmountProjectiles > 0 && other.GetComponent<HouseManager>().Player.Equals(this.PlayerNumber))
        {
            if (Input.GetButtonDown(string.Format("{0}_Action3", PlayerNumber)))
            {
                Debug.Log("_Action3 EGG");
                UpdateHouse(other);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name.Contains("Egg") && ProjectileTarget == other.gameObject)
        {
            ProjectileTarget = null;
            //Debug.Log("OnTriggerExit EGG");
        }
    }

    #endregion

    #region Actions

    private void Fire()
    {
        Ray ray = new Ray(aim.transform.position, aim.transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100, mask))
        {
            Debug.DrawLine(aim.transform.position, hit.transform.position, Color.red, 10);
            //Destroy(hit.collider.gameObject);
        }

        if (AmountProjectiles == 1)
        {
            animator.SetTrigger("throw");
            GameObject projectiles = eggProjectiles[0];
            projectiles.transform.SetParent(null);
            projectiles.GetComponent<Rigidbody>().isKinematic = false;
            projectiles.GetComponent<Rigidbody>().useGravity = true;
            projectiles.GetComponent<Projectile>().Fire(aim.transform.forward);
            eggProjectiles.Clear();
            AmountProjectiles--;
        }

    }

    private void GetProjectiles(Collider projectile)
    {
        if (AmountProjectiles == 1)
        {
            SetPivot(eggProjectiles[0], pivot2Egg[0]);
            projectile.transform.SetParent(this.transform);
            SetPivot(projectile.gameObject, pivot2Egg[1]);
        }
        else
        {
            projectile.transform.SetParent(this.transform);
            SetPivot(projectile.gameObject, pivot1Egg);
        }

        projectile.GetComponent<Projectile>().PlayerTarget = PlayerNumber;
        projectile.GetComponent<Rigidbody>().isKinematic = true;
        AmountProjectiles++;
        eggProjectiles.Add(projectile.gameObject);
        projectile.GetComponent<Projectile>().IsGround = false;

        Debug.Log(" Verificar qual projectileTarget :: " + ProjectileTarget);
        ProjectileTarget = null;
        Debug.Log(" Limpar projectileTarget :: " + ProjectileTarget);

    }

    private void UpdateHouse(Collider house)
    {
        house.GetComponent<HouseManager>().SetEgg(AmountProjectiles);
        AmountProjectiles = 0;

        foreach (GameObject proj in eggProjectiles)
        {
            RespawnManager.Instance.UseEgg(proj);
            Destroy(proj);
        }

        eggProjectiles.Clear();
    }

    public void Stun()
    {
        //Soltar ovos
        foreach (GameObject item in eggProjectiles)
        {
            item.transform.SetParent(null);
            item.GetComponent<Rigidbody>().isKinematic = false;
            item.GetComponent<Rigidbody>().useGravity = true;
            item.GetComponent<BoxCollider>().isTrigger = false;
            //item.GetComponent<Projectile>().Fire(aim.transform.forward);
            AmountProjectiles--;
        }

        eggProjectiles.Clear();

        GetComponent<ControllerManager>().CanMove = false;
        Debug.LogFormat(" (stunTimeMax - (stunTimeMax / stunResistence) :: {0}", (stunTimeMax - (stunTimeMax / stunResistence)));
        Debug.LogFormat(" (stunTimeMax [{0}] - (stunTimeMax [{0}] / stunResistence [{1}] ) :: {2}", stunTimeMax, stunResistence, (stunTimeMax - (stunTimeMax / stunResistence)));

        StartCoroutine(ReturnTime((stunTimeMax - (stunTimeMax / stunResistence)), new Action(() => GetComponent<ControllerManager>().CanMove = true)));

        if (stunCoroutine != null)
        {
            StopCoroutine(stunCoroutine);
        }
        stunCoroutine = StartCoroutine(StunResistenceTime_Coroutine());
    }

    #endregion

    #region Utils

    private IEnumerator StunResistenceTime_Coroutine()
    {
        yield return new WaitForSeconds(stunResistenceTime);
        stunResistence = 1;
    }

    private IEnumerator ReturnTime(float timeWait, Action callback)
    {
        yield return new WaitForSeconds(timeWait);

        if (callback != null)
        {
            callback();
        }
    }

    private void SetPivot(GameObject projectile, GameObject target)
    {
        projectile.transform.position = target.transform.position;
        projectile.transform.rotation = target.transform.rotation;
    }

    #endregion
    void OnCollisionEnter(Collision c)
    {

        if (c.gameObject.tag == "egg")
        {
            audioData.Play(0);
        }
    }
}
