﻿using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    #region Variables

    public static GameManager Instance;

    [Header("Settings")]
    public StatusGame statusGame;

    [SerializeField]
    private GameObject playerPrefab;

    [SerializeField]
    private List<HouseManager> spawnPlayerOrigin = new List<HouseManager>();

    [Header("Controllers")]
    [SerializeField]
    private List<HouseManager> spawnPlayer = new List<HouseManager>();
    private List<HouseManager> SpawnPlayer
    {
        get
        {
            if (spawnPlayer.Count == 0)
            {
                spawnPlayer = new List<HouseManager>(spawnPlayerOrigin);
            }

            return spawnPlayer;
        }
        set => spawnPlayer = value;
    }

    [SerializeField]
    private List<ControllerManager> players = new List<ControllerManager>();

    public List<ControllerManager> Players { get => players; set => players = value; }

    [SerializeField]
    private List<GameObject> prefabPlayers = new List<GameObject>();

    [SerializeField]
    private KeyCode keyCodeStartGame;

    [SerializeField]
    private GameObject ResetGamePanel;

    [SerializeField]
    private TextMeshProUGUI textFinal;

    [SerializeField]
    private Image imageFinal;

    #endregion

    private void Start()
    {
        Instance = this;
    }

    private void Update()
    {
        if (statusGame == StatusGame.EndGame)
        {
            if (Input.GetKeyDown(keyCodeStartGame))
            {
                ResetGame();
            }
        }
    }

    public void StartPlayer(List<SelectPlayer> selectPlayers)
    {
        SpawnPlayer.Clear();
        statusGame = StatusGame.Gameplay;
        RespawnManager.Instance.InitSpawn();

        foreach (SelectPlayer player in selectPlayers)
        {
            SetSpawn(player);
        }
    }

    public void SetSpawn(SelectPlayer player)
    {
        int random = Random.Range(0, SpawnPlayer.Count);
        Debug.LogFormat(" {0} - {1} ", SpawnPlayer.Count, random);

        SpawnPlayer[random].GetComponent<HouseManager>().InitHouse(player, player.PlayerIndex, player.ColorPlayer);
        SpawnPlayer[random].HouseScore = Score;

        GameObject spawnAux = SpawnPlayer[random].GetComponent<HouseManager>().spawn;

        GameObject playerClone = Instantiate(playerPrefab, spawnAux.transform.position, spawnAux.transform.rotation);

        SpawnPlayer.Remove(SpawnPlayer[random]);

        playerClone.GetComponent<ControllerManager>().PlayerController = player.PlayerIndex;
        playerClone.GetComponent<ControllerManager>().selectPlayer = player;
        Players.Add(playerClone.GetComponent<ControllerManager>());
    }

    //Verificar fim de jogo com action
    public void Score(int playerScore, SelectPlayer selectPlayer)
    {
        if (playerScore >= 10)
        {
            statusGame = StatusGame.EndGame;
            EndGame(selectPlayer);
        }
    }

    public void EndGame(SelectPlayer selectPlayer)
    {
        ControllerManager playerCM = Players.First(x => x.selectPlayer = selectPlayer);
        playerCM.GetComponent<PlayerManager>().animator.SetTrigger("win");
        ResetGamePanel.SetActive(true);
        textFinal.text = string.Format(" WIN PLAYER ");
        imageFinal.color = selectPlayer.ColorPlayer;
    }

    public void ResetGame()
    {
        SceneManager.LoadScene("Arena");
    }

}
