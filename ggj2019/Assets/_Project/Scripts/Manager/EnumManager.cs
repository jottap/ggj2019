﻿
/// <summary>
/// Status do game
/// </summary>
public enum StatusGame
{
    /// <summary>
    /// Menu Inicial
    /// </summary>
    MenuStart = 0,
    /// <summary>
    /// GamePlay
    /// </summary>
    Gameplay = 1,
    Tutorial = 2,
    EndGame = 3
}
