﻿using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    #region Variables

    public static TutorialManager Instance;

    [Header("Settings")]
    [SerializeField]
    public List<GameObject> tutoriaisPanel = new List<GameObject>();

    public int indexTutorial;

    #endregion

    private void Start()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        indexTutorial = 0;

        foreach (GameObject item in tutoriaisPanel)
        {
            item.SetActive(false);
        }

        tutoriaisPanel[0].SetActive(true);
    }

}
